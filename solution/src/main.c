#include <stdio.h>

#include "bmp_io.h"
#include "image.h"
#include "rotate.h"


extern const char* reading_messages[];
extern const char* writing_messages[];


static const char* wrong_args_count = "Wrong arguments count\nUse: ./image_transformer input_file output_file\n";
static const char* cant_open_input = "Can't open input file\n";
static const char* cant_open_output = "Can't open output file\n";
static const char* error_while_closing_input = "Error while closing input file\n";
static const char* error_while_closing_output = "Error while closing output file\n";

static void print_err(const char* error_text){
    fprintf(stderr, "%s", error_text);
}

static void print(const char* text){
    fprintf(stdout, "%s", text);
}


int main( int argc, char** argv ) {
    if (argc != 3) {
        print_err(wrong_args_count);
        return 255;
    }

    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");

    if(in == NULL)
    {
        print_err(cant_open_input);
        return 1;
    }
    if(out == NULL)
    {
        print_err(cant_open_output);
        if(fclose(in)){
            print_err(error_while_closing_input);
        }
        return 1;
    }

    struct image img = (struct image){0};

    enum read_status status = from_bmp(in, &img);
    if(status == READ_OK){
        struct image new_img = rotate(img);
        enum write_status ok = to_bmp(out, &new_img);

        free_image_memory(&img);
        free_image_memory(&new_img);

        if(ok != WRITE_OK){
            print_err(writing_messages[ok]);
            if(fclose(in)){
                print_err(error_while_closing_input);
            }
            if(fclose(out)){
                print_err(error_while_closing_output);
            }
            return 255;
        }
        else{
            print(writing_messages[ok]);
        }
    }
    else{
        print_err(reading_messages[status]);
    }

    if(fclose(in)){
        print_err(error_while_closing_input);
    }
    if(fclose(out)){
        print_err(error_while_closing_output);
    }

    return 0;
}
