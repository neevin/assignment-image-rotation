#include <malloc.h>
#include <stdio.h>

#include "image.h"


void free_image_memory(struct image* img) {
	free(img->data);
}

struct image image_create(uint64_t width, uint64_t height){
	struct pixel* new = malloc(sizeof(struct pixel) * (width * height));
	return (struct image) { .width = width, .height = height, .data = new };
}
