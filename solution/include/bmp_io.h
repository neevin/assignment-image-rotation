#ifndef BMP_IO_H
#define BMP_IO_H

#include "image.h"
#include <stdint.h>
#include <malloc.h>
#include <stdio.h>


#define BMP_FILE_SIGNATURE 0x4D42
#define BITS_PER_PIXEL 24
#define PLANES_NUM 1

#define HEADER_SIZE (sizeof( struct bmp_header ))
#define DIB_HEADER_SIZE 40

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits; // отступ в файле до битовой карты
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed)); // packed заставляет компилятор упаковывать поля плотно в памяти

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_BITS_OFFSET,
    READ_INVALID_HEADER
    /* коды других ошибок  */
  };

/*  serializer   */
enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);

#endif
