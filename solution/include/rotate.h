#ifndef ROTATE_H
#define ROTATE_H

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );

#endif
